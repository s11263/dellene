import implementations.RepositoryPet;
import implementations.RepositoryUser;

import java.sql.Connection;
import java.sql.DriverManager;

import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import Repositories.IRepository;
import domain.Pet;
import domain.User;
import entityBuilders.IEntityBuilder;
import entityBuilders.PetEntityBuilder;
import entityBuilders.UserEntityBuilder;


public class Main {

	public static void main(String[] args) {
		
		
		
		
		 try 
	        {
	        Class.forName("com.mysql.jdbc.Driver").newInstance();
	        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dellene", "root", "root");
	        IEntityBuilder<User> builder = new UserEntityBuilder();
	        IUnitOfWork uow = new UnitOfWork(connection);
	        IRepository<User> repo = new RepositoryUser(connection, builder, uow);
	        User u = new User();
	        u.setLogin("Malin");                     
	        u.setPassword("root123");
	        u.setEmail("nanana@wp.pl");
	        u.setName("Marta");
	        u.setSurname("Nana");
	        u.setCity("Warszawa");
	        u.setAge(20);
	        repo.add(u);		
	        
	        IEntityBuilder<Pet> builder1 = new PetEntityBuilder();
	        IRepository<Pet> repo1 = new RepositoryPet(connection, builder1, uow);
	        Pet p = new Pet();
	        p.setName("Azor");                     
	        p.setAge(5);
	        p.setBreed("kundel");
	        repo1.add(p);	
	        
	        
	      /*  User u1 = new User();
	        u1.setLogin("Cosik");                  
	        u1.setPassword("password");
	        repo.add(u1); */

	        uow.commit();

	        connection.close();
	        } 
	        catch (Exception e) 
	        {
	        e.printStackTrace();
	        }
	}
		

	}


