
/* wyszukiwanie usera poprzez np login, email, wiek, miasto itp */
package implementations;



import java.sql.Connection;
import java.sql.SQLException;

import unitOfWork.IUnitOfWork;
import domain.User;
import entityBuilders.IEntityBuilder;


public class RepositoryUser extends RepositoryBase<User>
{
	
	public RepositoryUser(Connection connection,IEntityBuilder<User> builder, IUnitOfWork uow) 
	{
		super(connection, builder, uow);
		
	}
	
	@Override
	protected void prepareUpdateQuery(User entity) throws SQLException
	{
		  	update.setLong(1, entity.getID());
	        update.setString(2, entity.getLogin());
	        update.setString(3, entity.getPassword());
	        update.setString(6, entity.getEmail());
	        update.setString(4, entity.getName());
	        update.setString(5, entity.getSurname());
	        update.setString(7, entity.getCity());
	        update.setInt(8, entity.getAge());

	}
	
	@Override
	protected void prepareAddQuery(User entity) throws SQLException 
	{
		save.setLong(1, entity.getID());
        save.setString(2, entity.getLogin());
        save.setString(3, entity.getPassword());
        save.setString(6, entity.getEmail());
        save.setString(4, entity.getName());
        save.setString(5, entity.getSurname());
        save.setString(7, entity.getCity());
        save.setInt(8, entity.getAge());

	}

	@Override
	protected String getTableName() {
		
		return "user";
	}

	@Override
	protected String getCreateQuery() 
	{
		return "INSERT INTO user (id, login, password, email, name, surname, city, age) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	}
	
	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE user SET login=?, password=?, email=?, name=?, surname=?, city=?, age=? WHERE id=?";
		 
	}


	
}
