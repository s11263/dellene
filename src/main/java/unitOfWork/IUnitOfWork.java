package unitOfWork;

import domain.Entity;

public interface IUnitOfWork 
{

	public void commit();
	public void rollback();
	
	public void markAsNew(Entity entity, IUnitOfWorkRepository repository);
	public void markAsChanged(Entity entity, IUnitOfWorkRepository repository);
	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);
	
}