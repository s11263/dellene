package entityBuilders;
import domain.Pet;
import java.sql.ResultSet;

public class PetEntityBuilder implements IEntityBuilder<Pet>{
	
	@Override
	public Pet build(ResultSet rs) {
		 try
	        {
			 Pet p = new Pet();
				p.setName(rs.getString("Name"));
				p.setAge(rs.getInt("age"));
				p.setBreed(rs.getString("breed"));
				p.setSpecies(rs.getString("species"));


				return p;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }

}
