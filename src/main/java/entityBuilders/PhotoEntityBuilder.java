package entityBuilders;
import domain.Photo;
import java.sql.ResultSet;

public class PhotoEntityBuilder implements IEntityBuilder<Photo> {

	@Override
	public Photo build(ResultSet rs) {
		 try
	        {
			 Photo ph = new Photo();
				ph.setDescription(rs.getString("Description"));
				ph.setDate(rs.getInt("date"));
				
	            return ph;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
	
}
