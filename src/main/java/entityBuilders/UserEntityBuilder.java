package entityBuilders;
import domain.User;
import java.sql.ResultSet;

public class UserEntityBuilder implements IEntityBuilder<User>{

	@Override
	public User build(ResultSet rs) {
		 try
	        {
	        	User u = new User();
				u.setLogin(rs.getString("Login"));
				u.setPassword(rs.getString("Password"));
				u.setEmail(rs.getString("Email"));
				u.setName(rs.getString("Name"));
				u.setSurname(rs.getString("Surname"));
				u.setCity(rs.getString("City"));
				u.setAge(rs.getInt("Age"));
				
				
	            return u;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	        }
	        return null;
	    }
	}

	

