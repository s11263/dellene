package servlets;

import implementations.RepositoryUser;

import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Repositories.IRepository;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import entityBuilders.IEntityBuilder;
import entityBuilders.UserEntityBuilder;

/**
 * Servlet implementation class User
 */
@WebServlet("/User")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dellene", "root", "root");
			IEntityBuilder<domain.User> builder = new UserEntityBuilder();
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepository<domain.User> repo = 
					new RepositoryUser(connection,builder,uow);
			
			List<domain.User> users = repo.getAll();
			
			String html = "<html><body>";//</body></html>";
			
			for(domain.User u : users)
			{
				html+=u.getLogin() + " " + u.getPassword() + "<br>";
			}
			
			html+="</body></html>";
			
			
			PrintWriter out = response.getWriter();
			out.println(html);
			out.close();
			}catch(Exception ex)
			{}
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
