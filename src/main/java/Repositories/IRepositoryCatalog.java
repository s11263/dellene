
package Repositories;

public interface IRepositoryCatalog {
    
	
    public IRepositoryPhoto getPhotos();
    public IRepositoryPet getPets();
    public IRepositoryUser getUsers();
    
}
