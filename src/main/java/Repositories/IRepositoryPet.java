/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Repositories;

import domain.Pet;

public interface IRepositoryPet extends IRepository<Pet>
{
 
	public Pet getByName(String name);
	public Pet getByAge(int age);
	public Pet getByBreed(String breed);
	public Pet getBySpecies(String species);
	
    
}
