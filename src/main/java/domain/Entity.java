/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package domain;

public abstract class Entity {
    
	protected int id;
	protected EntityState state;
    
    public int getID() 
    {
		return id;
	}
    
    public EntityState getState()
	{
		return state;
	}

	public void setState(EntityState state)
	{
		this.state = state;
	}
    
}
