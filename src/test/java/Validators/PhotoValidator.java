/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Validators;
import domain.*;
public class PhotoValidator {
    
    public static Photo createPhoto( int id,
			int date,
                        String description){
		
		Photo createPhoto = new Photo();
		
		// createPhoto.setID(id);
		createPhoto.setDate(date);
		createPhoto.setDescription(description);
                
		return createPhoto;
	}
    
}
